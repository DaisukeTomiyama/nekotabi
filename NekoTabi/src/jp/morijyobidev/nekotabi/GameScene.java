package jp.morijyobidev.nekotabi;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;

import android.graphics.PointF;
import android.os.Debug;
import android.util.Log;
import android.view.KeyEvent;

public class GameScene extends KeyListenScene implements IOnSceneTouchListener {
	
	
	// 猫Sprite
	private Neko mNeko;
	private boolean isTouched;
	private float mTouchedX, mTouchedY;
	

	public GameScene(MultiSceneActivity baseActivity) {
		super(baseActivity);
		init();
	}

	@Override
	public void init() {
		 ResourceUtil util = getBaseActivity().getResourceUtil();
		 Camera camera = getBaseActivity().getEngine().getCamera();
		
		// 背景画像
		Sprite backSprite =  util.getSprite("background.png");
		attachChild(backSprite);
		
		// 猫画像
		mNeko = new Neko( util.getAnimatedSprite("neko.png", 1, 4) );
		// キャラクターのアニメーション設定
		mNeko.getSprite().stopAnimation(0);
		mNeko.setPositionFromCenter( new PointF(camera.getWidth()*0.5f, camera.getHeight()*0.7f) );
		attachChild(mNeko.getSprite());
		
		// パラメタの初期化
		isTouched = false;
		mTouchedX = mTouchedY = 0.0f;
		
		setOnSceneTouchListener(this);
		registerUpdateHandler(updateHander);
	}
	
	// 秒間６０フレームでメインループ
	public TimerHandler updateHander = new TimerHandler(1f / 60f, true,
			new ITimerCallback() {
		@Override
		public void onTimePassed(TimerHandler pTimerHandler) {
			
			
			
		}
	});

	

	@Override
	public void prepareSoundAndMusic() {

	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		return false;
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if ( pSceneTouchEvent.isActionDown() ) {
			// 指が画面に触れている間アニメーションの再生
			mNeko.getSprite().animate(new long[]{200, 200, 200}, 0, 2, true);
			isTouched = true;
		}
		
		if ( pSceneTouchEvent.isActionUp() ) {
			// 指が画面から離された時アニメーションの停止
			mNeko.getSprite().stopAnimation(0);
			isTouched = false;
		}
		
		if ( pSceneTouchEvent.isActionMove() ) {
			// タッチされている座標が変更された時(指を動かした)
			if (mNeko.getCenterX() < pSceneTouchEvent.getX()) {
				// 猫の中心よりも指が右側に触れている時Spriteを反転
				mNeko.setDirection(mNeko.D_RIGHT);
				Log.d("NekoCenter", "x:" + mNeko.getCenterX() + "px y:" + mNeko.getCenterY());
				Log.d("NekoPosition", "left:" + mNeko.getLeft() + "px top:" + mNeko.getTop());
			} else {
				// 猫の中心よりも指が左側に触れているときSpriteを反転解除
				mNeko.setDirection(mNeko.D_LEFT);
			}
			// 座標をメンバに格納
			mTouchedX = pSceneTouchEvent.getX();
			mTouchedY = pSceneTouchEvent.getY();
		}
		
		if ( pSceneTouchEvent.isActionOutside() ) {
			// 指が画面からはみ出した時にアニメーションの停止
			mNeko.getSprite().stopAnimation(0);
			isTouched = false;
		}
		return true;
	}
		
	

}


class Neko {
	
	public static final int D_LEFT = -1, D_RIGHT = 1;
	private static final float SPEED = 2.0f;
	
	private AnimatedSprite mSprite;
	private int mDirection = D_LEFT;
	
	public Neko ( AnimatedSprite sprite ) {
		this.mSprite = sprite;
	}
	
	
	public void setPositionFromCenter ( PointF point ) {
		this.mSprite.setPosition(point.x - mSprite.getWidth()/2, point.y - mSprite.getHeight()/2);
	}
	
	public void setPositionFromTopLeft ( PointF point ) {
		this.mSprite.setPosition(point.x, point.y);
	}
	
	public void setDirection ( int direction ) {
		if ( direction == D_LEFT ) {
			direction = D_LEFT;
			mSprite.setFlippedHorizontal( false );
		} else if ( direction == D_RIGHT ) {
			direction = D_RIGHT;
			mSprite.setFlippedHorizontal( true );
		}
	}
	
	public int getDirection () {
		return this.mDirection;
	}
	
	public AnimatedSprite getSprite() {
		return this.mSprite;
	}
	
	public float getCenterX () {
		return this.mSprite.getX() + this.mSprite.getWidth()/2;
	}
	
	public float getCenterY () {
		return this.mSprite.getY() + this.mSprite.getHeight()/2;
	}
	
	public float getLeft () {
		return this.mSprite.getX();
	}
	
	public float getTop () {
		return this.mSprite.getY();
	}
	
	public float getRight () {
		return this.mSprite.getX() + mSprite.getWidthScaled();
	}
	
	public float getBottom () {
		return this.mSprite.getY() + mSprite.getHeightScaled();
	}
}


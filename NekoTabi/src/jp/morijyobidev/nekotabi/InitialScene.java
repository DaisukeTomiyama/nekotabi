package jp.morijyobidev.nekotabi;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;

import android.view.KeyEvent;
import android.widget.Toast;

public class InitialScene extends KeyListenScene implements IOnAreaTouchListener {
	
	public static final String FONT_NAME = "KajudenFont-Crayon.ttf";
	private Text mMenuText;

	public InitialScene(MultiSceneActivity baseActivity) {
		super(baseActivity);
		init();
	}

	@Override
	public void init() {
		 ResourceUtil util = getBaseActivity().getResourceUtil();
		 Camera camera = getBaseActivity().getEngine().getCamera();
		
		// 背景画像
		Sprite backSprite =  util.getSprite("background.png");
		attachChild(backSprite);
		
		// フォントの取得
		Font titleFont = util.getFont(FONT_NAME, 96, android.graphics.Color.RED);
		Font titleBackFont = util.getFont(FONT_NAME, 100, android.graphics.Color.WHITE);
		Font menuFont = util.getFont(FONT_NAME, 56, android.graphics.Color.RED);
		// 文字列の描画
		Text titleText = new Text(0, 0, titleFont, "ねこたび", getBaseActivity().getVertexBufferObjectManager());
		Text titleBackText = new Text(0, 0, titleBackFont, "ねこたび", getBaseActivity().getVertexBufferObjectManager());		
		mMenuText = new Text(0, 0, menuFont, "げーむすたーと", getBaseActivity().getVertexBufferObjectManager());		
		titleText = placeToCenterX(titleText, camera.getHeight()*0.2f);
		titleBackText = placeToCenterX(titleBackText, camera.getHeight()*0.2f);	
		mMenuText = placeToCenterX(mMenuText, camera.getHeight()*0.7f);
		attachChild(titleBackText);
		attachChild(titleText);
		attachChild(mMenuText);
		registerTouchArea(mMenuText);
		
		
		// タッチエリアのリスナーをシーンに登録
		setOnAreaTouchListener(this);
	}

	@Override
	public void prepareSoundAndMusic() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			ITouchArea pTouchArea, float pTouchAreaLocalX,
			float pTouchAreaLocalY) {
		if ( pSceneTouchEvent.isActionUp() ) {
			if ( pTouchArea == mMenuText ) {
				getBaseActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getBaseActivity(), "ゲームスタートが押された", Toast.LENGTH_SHORT).show();
					}
				});
				ResourceUtil.getInstance(getBaseActivity()).resetAllTexture();
				KeyListenScene scene = new GameScene(getBaseActivity());
				getBaseActivity().getEngine().setScene(scene);
				getBaseActivity().appendScene(scene);
			}
		}
		
		
		return true;
	}

}

package jp.morijyobidev.nekotabi;

import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.font.FontFactory;

import android.view.KeyEvent;


public class MainActivity extends  MultiSceneActivity {

	// ===================================================
	// 定数
	// ===================================================
	private static final int CAMERA_WIDTH = 480;
	private static final int CAMERA_HEIGHT = 800;
	
	
	// ===================================================
	// フィールド
	// ===================================================
	
	private Camera mCamera;

	/*
	 * (non-Javadoc)
	 * @see org.andengine.ui.IGameInterface#onCreateEngineOptions()
	 */
	@Override
	public EngineOptions onCreateEngineOptions() {
		// カメラの初期化
		mCamera = new Camera(0,  0, CAMERA_WIDTH, CAMERA_HEIGHT);
		// ゲームエンジンの初期化処理
		EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.PORTRAIT_FIXED, new RatioResolutionPolicy(
						CAMERA_WIDTH, CAMERA_HEIGHT), mCamera);
		// 効果音の使用を許可する
		engineOptions.getAudioOptions().setNeedsSound(true);
		// Andengineのインスタンス化
		return engineOptions;
	}

	@Override
	protected Scene onCreateScene() {
		// サウンドファイルの格納場所を指定
		SoundFactory.setAssetBasePath("mfx/");
		FontFactory.setAssetBasePath("font/");
		//  初期画面を追加
		// InitialSceneをインスタンス化し、エンジンにセット
		InitialScene initialScene = new InitialScene(this);
		// 遷移管理用配列に追加
		getSceneArray().add(initialScene);
		return initialScene;
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void appendScene(KeyListenScene scene) {
		getSceneArray().add(scene);
		
	}

	@Override
	public void backToInitial() {
		// 遷移管理用配列をクリア
		getSceneArray().clear();
		// 新たにInitialSceneからスタート
		KeyListenScene scene = new InitialScene(this);
		getSceneArray().add(scene);
		getEngine().setScene(scene);
		
	}

	@Override
	public void refreshRunningScene(KeyListenScene scene) {
		// 配列の最後の要素を削除し、新しいものに入れ替える
		getSceneArray().remove(getSceneArray().size() -1);
		getSceneArray().add(scene);
		getEngine().setScene(scene);
		
	}

	@Override
	protected int getLayoutID() {
		// Activityのレイアウトidを返却する
		return R.layout.activity_main;
	}

	@Override
	protected int getRenderSurfaceViewID() {
		// SceneがセットされるViewのIDを返却
		return R.id.renderview;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// バックボタンが押された時
		if (event.getAction() == KeyEvent.ACTION_DOWN 
				&& event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			// 起動中のdispatchKeyEventを呼び出す.追加の処理が必要な時はfalseが返却されるため処理をする
			if (!getSceneArray().get(getSceneArray().size() - 1)
					.dispatchKeyEvent(event)) {
				// Sceneが一つしか起動していない時はゲームを終了する。
				if (getSceneArray().size() == 1 ) {
					// リソースの解放
					ResourceUtil.getInstance(this).resetAllTexture();
					// Activityの終了
					finish();
				}
				// 複数のSceneが立ち上がっている時は一つ前のScxeneに戻る
				else {
					getEngine().setScene(getSceneArray().get(getSceneArray().size()-2));
					getSceneArray().remove(getSceneArray().size()-1);
				}
			}
			return true;
		}else if (event.getAction() == KeyEvent.ACTION_DOWN
				&& event.getKeyCode() == KeyEvent.KEYCODE_MENU) {
			getSceneArray().get(getSceneArray().size() - 1).dispatchKeyEvent(event);
			return true;
		}
		return false;
	}

	


}
